const people = require("./people");
const { includes, indexOf, push, values } = require("./people");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let x = [];
    for (let i of p) {
      if (i.gender === "Male") {
        x.push(i);
      }
    }
    return x;
  },

  allFemale: function (p) {
    let x = [];
    for (let i of p) {
      if (i.gender === "Female") {
        x.push(i);
      }
    }
    return x;
  },

  nbOfMale: function (p) {
    let n = 0;
    for (x of p) {
      if (x.gender === "Male") n++;
    }
    return n;
  },
  nbOfFemale: function (p) {
    return p.length - this.nbOfMale(p);
  },

  nbOfMaleInterest: function (p) {
    let i = 0;
    for (x of p) {
      if (x.looking_for === "M") i++;
    }
    return i;
  },

  nbOfFemaleInterest: function (p) {
    let i = 0;
    for (x of p) {
      if (x.looking_for === "F") i++;
    }
    return i;
  },

  nbOfplusde2000: function (p) {
    let nb = 0;
    for (let i of p) {
      if (parseFloat(i.income.substring(1)) > 2000) {
        nb++;
      }
    }
    return nb;
  },

  nbdramaaddict: function (p) {
    let i = 0;
    for (x of p) {
      if (x.pref_movie === "Drama") i++;
    }
    return i;
  },
  nbfemmessf: function (p) {
    let i = 0;
    for (x of this.allFemale(p)) {
      if (x.pref_movie.includes("Sci-Fi")) i++;
    }
    return i;
  },

  nbpdoc1482: function (p) {
    let i = 0;
    for (x of p) {
      if (x.pref_movie.includes("Documentary"))
        if (parseFloat(x.income.substring(1)) > 1482) i++;
    }
    return i;
  },

  list4000: function (p) {
    let n = [{ id: "", first_name: "", last_name: "" }];
    for (let x of p)
      if (parseFloat(x.income.substring(1)) > 4000) {
        n.push(x.id, x.first_name, x.last_name, x.income);
      }
    return n;
  },
  leplusriche: function (p) {
    let lpr;
    let s = 0;
    for (let x of this.allMale(p))
      if (parseFloat(x.income.substring(1)) > s) {
        s = parseFloat(x.income.substring(1));
        lpr = [x.last_name, x.id];
      }

    return lpr;
  },

  salairemoyen: function (p) {
    let i = [];
    const reducer = (previousValue, currentValue) =>
      previousValue + currentValue;

    for (let x of p) {
      let salaire = parseFloat(x.income.substring(1));
      i.push(salaire);
    }
    return i.reduce(reducer) / p.length;
  },

  salairemedian: function (p) {
    let i = [];

    for (let x of p) {
      let salaire = parseFloat(x.income.substring(1));
      i.push(salaire);
    }
    i.sort(function (a, b) {
      return a - b;
    });

    if (i.length % 2 !== 0) {
      return i[i.length / 2];
    } else {
      return (i[i.length / 2] + i[i.length / 2 + 1]) / 2.0;
    }
  },

  phemisnord: function (p) {
    let i = 0;
    for (x of p) {
      if (x.latitude > 0) {
        i++;
      }
    }
    return i;
  },

  salairemoyenhs: function (p) {
    let nb = [];
    let ns = 0;
    const reducer = (previousValue, currentValue) =>
      previousValue + currentValue;
    for (let i of p) {
      if (i.latitude < 0) {
        let doll$ = parseFloat(i.income.substring(1));
        nb.push(doll$);
        ns++;
      }
    }
    return nb.reduce(reducer) / ns;
  },

  pythagore: function (a, b) {
    return Math.sqrt(Math.pow(b.long - a.long, 2) + Math.pow(b.lat - a.lat, 2));
  },

  berenicenear: function (p) {
    let a = { long: 0, lat: 0 };
    for (let x of p) {
      if (x.last_name == "Cawt" && x.first_name == "Bérénice") {
        a.long = x.longitude;
        a.lat = x.latitude;
      }
    }
    let gap = 999999999999.0;
    let nearest_User = [];
    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }

    return nearest_User;
  },

  brrui: function (p) {
    let a = { long: 0, lat: 0 };
    for (let x of p) {
      if (x.last_name == "Brach" && x.first_name == "Ruì") {
        a.long = x.longitude;
        a.lat = x.latitude;
      }
    }
    let gap = 999999999999.0;
    let nearest_User = [];

    for (let i of p) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }

    return nearest_User;
  },

  multi: function (p) {
    let dist = [];
    let latBoshard = 57.6938555;
    let longBoshard = 11.9704401;
    for (let x of p) {
      let a = latBoshard - x.latitude;
      let b = longBoshard - x.longitude;
      let distance_entre_eux = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
      dist.push({ distance: distance_entre_eux, nom: x.last_name, id: x.id });
      dist.sort((a, b) => a.distance - b.distance);
    }

    let lesdix = [];
    for (i = 1; i < 11; i++) {
      lesdix.push(dist[i]);
    }
    return lesdix;
  },

  gwork: function (p) {
    let t = [];
    for (let i of p) {
      if (i.email.includes("google")) {
        t.push(i.id, i.last_name);
      }
    }
    return t;
  },

  oldpeople: function (p) {
    let t = [];
    for (let i of p) {
      t.push({ ddn: new Date(i.date_of_birth), name: i.last_name });
      t.sort((a, b) => a.ddn - b.ddn);
    }
    return t[0];
  },

  plusjeune: function (p) {
    let t = [];
    for (i of p) {
      new Date(i.date_of_birth);
      t.push({ ddn: new Date(i.date_of_birth), name: i.last_name });
      t.sort((a, b) => b.ddn - a.ddn);
    }
    return t[0];
  },

  ecartage: function (p) {
    age = Math.floor((new Date() - new Date(i.date_of_birth)) / 31557600000);

    let agepeople = [];
    let moyenneecart;
    let ecart;

    for (let i of p) {
      agepeople.push(
        (age = Math.floor(
          (new Date() - new Date(i.date_of_birth)) / 31557600000
        ))
      );
    }
    let diff = [];
    for (let i of agepeople) {
      agepeople.splice(agepeople.indexOf(i), 1);

      for (let f of agepeople) {
        diff.push(Math.abs(i - f));
      }
    }

    ecart = 0;
    for (let i of diff) {
      ecart += i;
    }

    moyenneecart = ecart / diff.length;

    return moyenneecart;
  },
  

  filmpop: function (p) {
    let tab = [];

    for (let i of p) {
      tab.push(i.pref_movie.split("|"));
    }
    tab = tab.reduce(function (a, b) {
      return a.concat(b)})
    
        var counter={}
        for ( var i=0;i<tab.length;i++){
          if (counter[tab[i]])
          counter[tab[i]]++
          else counter[tab[i]]=1;}
          
          let sortable = [];
          for (let i in counter) {
              sortable.push([i, counter[i]]);
          }
          
          sortable.sort(function(a, b) {
              return a[1] - b[1];
          });

             return sortable.slice(-1)[0] 
     
    },


   liste: function (p) {
    let tab = [];

    for (let i of p) {
      tab.push(i.pref_movie.split("|"));
    }
    tab = tab.reduce(function (a, b) {
      return a.concat(b)})
    
        var counter={}
        for ( var i=0;i<tab.length;i++){
          if (counter[tab[i]])
          counter[tab[i]]++
          else counter[tab[i]]=1;}
          
          let sortable = [];
          for (let i in counter) {
              sortable.push([i]);
          }
          
          sortable.sort(function(a, b) {
              return b[1] - a[1];
          });

          return sortable
              
    },

    listeetnombre:function(p){
      let tab = [];

      for (let i of p) {
        tab.push(i.pref_movie.split("|"));
      }
      tab = tab.reduce(function (a, b) {
        return a.concat(b)})
      
          var counter={}
          for ( var i=0;i<tab.length;i++){
            if (counter[tab[i]])
            counter[tab[i]]++
            else counter[tab[i]]=1;}
            
            let sortable = [];
            for (let i in counter) {
                sortable.push([i, counter[i]]);
            }
            
            sortable.sort(function(a, b) {
                return a[1] - b[1];
            });
  
               return sortable
       
      },

    

      agemoyenfilmnoir:function(p){
        let hfm=[]
        for(let h of p)
        if(h.gender==="Male"&& h.pref_movie==="Film-Noir"){
          hfm.push(h.date_of_birth)
        }

return hfm
      },


    

  

  match: function (p) {
    return "not implemented".red;
  }
};
